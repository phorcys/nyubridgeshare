# Internet Protocol

## Goals

- Identify characteristics and purpose of IPv4
- Describe IPv4 header fields
- Identify the benefits of using classless inter-domain routing (CIDR)
- Identify special addresses associated with IPv4
- Calculate subnet masks
- Describe the process of packet fragmentation
- Demonstrate packet decoding
- Use BPF Filters for minimizing network traffic data

## Internet Protocol

- Link: [IP Header](https://nmap.org/book/images/hdr/MJB-IP-Header-800x576.png)
- Like a post office
- assign addresses to devices
- Routing to deliver packets

  - Moves between routers/post offices

- No authority that defines path

- each router makes the decision based on environment

## characteristics

- unreliable

  - no guarantee packet will make it to Destination

- connectionless

  - protocol does not keep connection state information
  - each packet on its own

    - packets can arrive out of order

  - packets can take different paths

## IPv4

- Addressing

  - ID hosts
  - Provides logical location services

- Address space

  - 32 binary bit space
  - four bytes
  - each section between dots is called octet (8 bits each section)
  - 4.3 billion address

    - ineffeciency in allocation of addresses over time
    - IPv6 as replacement

      - 128 bits per address

## IP Header

- 2 components

  - Called encapsulation
  - header

    - <https://nmap.org/book/images/hdr/MJB-IP-Header-800x576.png>
    - source IP address
    - Destination IP address
    - Other metadata

  - Payload

    - Transported data

- Parts of the Header

  - Version

    - The first field in the IP header is the Version field. For IPv4, this has a value of 4 (hence the name IPv4). The field has a value of 6 for IPv6.

  - Internet Header Length (IHL) (field size is 4 bits):

    - The Internet Header Length (IHL) field indicates the number of 4-byte blocks in the IPv4 header. The IPv4 header has a minimum of 20 bytes in size (the smallest value of the IHL field is 5) to a maximum of 60 bytes.

  - Type of Service (field size is 8 bits):

    - The primary purpose of this field is to provide a means of prioritizing datagrams that are waiting to pass through a router. Most implementations of IP today simply put all 0s in this field. RFC 2474 provides the modern definition as the Differentiated Services (DS) field. The high-order 6 bits of the DS field comprise the DS Code Point (DSCP) field. The DSCP field allows devices in a network to mark, unmark, and classify packets for forwarding. This is usually done based on the needs of an application. For example, Voice over IP and other real-time packets take precedence over e-mail in congested areas of the network. This is commonly referred to as Quality of Service (QoS). Low-order 2 bits of the Type of Service field are used for Explicit Congestion Notification (ECN), as defined in RFC 3168.

  - Total Length:

    - The Total Length field indicates the length of the IPv4 packet, measured in octets, including internet header and data.

  - Identification:

    - This field is primarily used for uniquely identifying the group of fragments of a single IP datagram to aid in assembling the fragments of a packet. Some experimental work has suggested using the ID field for other purposes, such as for adding packet-tracing information to help trace datagrams with spoofed source addresses, but RFC 6864 now prohibits any such use.

  - Flags (field size is 3 bits):

    - The Flags field identifies flags for the fragmentation process. The size of this field is 3 bits; however, only 2 bits are defined for current use. There are two flags: one to indicate whether the IPv4 packet can be fragmented, which is the DF (or Don't Fragment) flag; and another to indicate whether more fragments follow the current fragment, which is MF (or More Fragments) flag. We will discuss fragmentation later in this module.

  - Fragment Offset (field size is 13 bits):

    - The Fragment Offset field indicates the position of the fragment relative to the beginning of the original IPv4 payload. We will discuss fragmentation later in this module.

  - Time to Live:

    - This field indicates the maximum time (measured in hops) the datagram is allowed to remain in the internet system. If this field contains the value zero, then the datagram must be destroyed. Different operating systems use different default values, which can be helpful when trying to guess a user's operating systems and device types: in UNIX, the default initial value is 64; Windows is 128; and Networking devices such as Cisco is 255.

  - Protocol (field size is 8 bits):

    - The Protocol field identifies the upper-layer protocol that is to receive the IPv4 packet payload. For example, a value of 6 in this field identifies TCP as the upper-layer protocol; a decimal value of 17 identifies UDP, and a value of 1 identifies ICMPv4.

  - Header Checksum (field size is 16 bits):

    - This field provides a checksum on the IPv4 header only. A checksum is a count of the number of bits in a transmission unit that is included with the unit so that the receiver can check to see whether the same number of bits arrived. If the counts match, it is assumed the complete transmission was received. Each IPv4 node that receives IPv4 packets verifies the IPv4 header checksum and silently discards the IPv4 packet if checksum verification fails. The IPv4 payload is not included in the checksum calculation because the IPv4 payload usually contains its own checksum.

  - Source Address (field size is 32 bits):

    - The Source Address field stores the IPv4 address of the originating host.

  - Destination Address (field size is 32 bits):

    - The Destination Address field stores the IPv4 address of an intermediate destination (in the case of source routing) or the destination host.

  - Options (field size is a multiple of 32 bits [or 4 bytes]):

    - The Options field stores one or more IPv4 options. Examples of options include timestamp, traceroute, and IP router alert. If an IPv4 option does not use all 32 bits, padding options must be added so that the IPv4 header is an integral number of 4-byte blocks that can be indicated by the IHL field. The internet header padding is used to ensure that the internet header ends on a 32 bit boundary.

- RFCs:

  - A Request for Comments (RFC) is a publication of the Internet Engineering Task Force (IETF) and the Internet Society, the principal technical development and standards-setting bodies for the Internet.
  - An RFC is authored by engineers and computer scientists in the form of a memorandum describing methods, behaviors, research, or innovations applicable to the working of the Internet and Internet-connected systems. It is submitted either for peer review or simply to convey new concepts, information, or (occasionally) engineering humor. The IETF adopts some of the proposals published as RFCs as Internet standards.

## IP Addressing

- Links

  - [IP Calculator](http://jodies.de/ipcalc)
  - [Subnet Cheatsheet](http://subnettingpractice.com/images/IPv4_cheat_sheet.png)
  - [Subnet Cheatsheet 2](http://i.imgur.com/yZAiw.png)
  - [Cisco routing page](http://www.cisco.com/c/en/us/support/docs/ip/routing-information-protocol-rip/13790-8.html)

- 4 Octets divided into 2 Parts

  - network segment
  - Host ID segment

- Classful Addressing

  - 5 address classes

    - Class A

      - 8 bits for network
      - 24 bits for host ID
      - 0-127 in first 8 bits

    - Class B

      - 16 bits for network
      - 16 bits for host ID
      - 128-191 in first 8 bits

    - Class C

      - 24 bits for network
      - 8 bits for host ID
      - 192-223 in first 8 bits

    - Class D

      - Reserved for multicast
      - 224-239 in first 8 bits

    - Class E

      - experimental
      - 240-255 in first 8 bits

- Classless interdomain routing

  - CIDER based on VLSM
  - Can no longer determine network ID with the address alone
  - Now uses Subnet mask

    - 32 bit binary to indicate network portion

      - Calculate with logical AND operation on IP address AND subnet mask

  - CIDR Notification

    - Helps reduce size of address tables
    - 1 indicates bit belongs to network
    - 0 indicates bit belongs to host
    - CIDR uses /x to determine number of 1 bits starting from left
    - If no netmask is given, it assumes CIDR based on first octets

- Subnetting

  - Move network and host boundry to right
  - Addressed in future modules

- Special addresses

  - Broadcast:

    - The broadcast is used for communication that is limited to only the hosts on the local network. This address is formed by setting all 32 bits of the IPv4 address to 1 (255.255.255.255). The limited broadcast address is used for one-to-everyone delivery on the local subnet when the local network ID is unknown. IPv4 nodes typically only use the limited broadcast address during an automated configuration process such as Bootstrap Protocol (BOOTP) or DHCP. For example, with DHCP, a DHCP client must use the limited broadcast address for all traffic sent until the DHCP server acknowledges the use of the offered IPv4 address configuration. IPv4 routers do not forward limited broadcast packets.

  - Unspecified Address:

    - An Unspecified Address is typically used as the source address for packets that are attempting to verify the uniqueness of the tentative unicast address. It's neither assigned to any interface nor used as the destination address. When a host computer starts up, the DHCP client software sends a special broadcast packet, known as a DHCP Discover message. This message uses the subnet's broadcast address (all host ID bits set to one) as the destination address and 0.0.0.0 as the source address.

  - Loopback:

    - It is not unusual for a host to use TCP/IP sockets internally to implement interprocess communication. To facilitate this use and to ensure a machine always has an IP address that it can use to communicate with itself, the IP address scheme has a concept of a loopback address. A loopback address, which by standard lies in the 127.0.0.0/8 network range, instructs the system to transmit data to itself. This loopback address is automatically assigned to a host when TCP/IP is running, almost always to 127.0.0.1/. Note that this address is locally significant; every TCP/IP host thinks it is the sole owner of its loopback address.

  - Private address space:

    - Private addressing allows organizations to use a large address space internally on their network while using only a few public addresses, rather than using only public addresses. The approved private addresses in IPv4 are 10.0.0.0/8, 172.16.0.0/12 (usually subnetted to /16), and 192.168.0.0/16 (usually subnetted to /24). Generally organizations will not route these addresses outside of their network; it is possible, but once you send packets with these addresses outside of your network control, you have no idea what your ISP will do with them.

  - Link-Local:

    - A link-local address is allocated for communication between hosts on a single network. The allocated range for link-local addresses is 169.254.0.0/16/. Windows hosts will autoconfigure a link-local address if it is configured to get an IP address via DHCP, but fails. UNIX hosts generally do not assign link-local addresses, but they can be manually configured.

## Exercises

- First one is VLSM

  - use [IP Calculator](http://jodies.de/ipcalc), [Subnet Cheatsheet](http://subnettingpractice.com/images/IPv4_cheat_sheet.png), [Subnet Cheatsheet 2](http://i.imgur.com/yZAiw.png)

- 2nd one is Troubleshooting

  - Limit Wireshark to source host with BPF filter `src host 192.168.1.1`
  - Capture specific host with tcpdump

    - `tcpdump -w file dst host 192.168.1.1`

  - view dump file with some options

    - `tcpdump -qnnXX -r filename`

  - Determine mac table

    - `arp -an`

  - Change IP address with sed

    - `sed -i 's/IPADDR=192.168.11.213/IPADDR=192.168.11.13/' /etc/sysconfig/network-scripts/ifcfg-eth0`
    - Then run `service network reload`

  - In the case of the exercise, one believed it was in a different network and send to the default gateway, the other believed it was on the same network and just sent directly

## Fragmentation

- MTU

  - Maximum transmission unit.
  - Size in bytes of largest packet the network can handle
  - Sending large MTU to small MTU

    - Must keep track of packets so they can be reassembled
    - Header components are ID, Flags, Frag Offset

- ID Field

  - Always same for same packet

- Flags field

  - 1st Bit Not used - 0
  - 2nd Bit DF or Don't Fragment flag - 0 means allowed, 1 means not allowed
  - 3rd bit MF or More Fragments 0 - no more, 1 - More Fragments

- Fragment Offset

  - Position of fragment relative to beginning
  - 8 bytes 0 - 8191
  - IP fragment Offset

    - specified in units of 8
    - multiples of 8 so take mtu - 20 for header and then divide by 8

- Possibilities

  - MF OFF + OFFSET 0 = Whole packet
  - MF ON + OFFSET 0 = First fragment
  - MF ON + OFFSET Non-0 = Middle Fragment
  - MF OFF + OFFSET Non-0 = Last Fragment

- Exercise

  - When asking for protocol information

    - MAC, IP, TTL to determine OS, TTL also helps ID if on a different network, OUI of MAC address to determine vender, Type of packet (in case of example, echo reply)

  - Read carefully ICMP Data, not 1500 MTU, 1480 due to header, last fragment was only 968

  - To check if fragment is missing, wireshark won't assemble packets that are missing fragments, or check offsets to make sure nothing is missing

  - ICMP header is in first fragment.

- Exercise 2 Packet decoding

  - [PhD tool](http://sadjad.me/phd)

## Sniffing

- sniffer must be on same shared network segment as data that you desire capturing
- sniffers installed on most rootkits
- rootkits also install other software
- sniffers can capture everything transfered over the network
- data must run by network interface the sniffer is listening on

- Most common filters on IP are source and network addresses

  - ip src - source
  - ip dst - destination
  - ip host source or destination ip addresses
  - BPF filters for networks

    - CIDR needed for Classless network otherwise assumes Classful

  - Older syntax

    - have to use ip[offset] for the last byte of subnet part of ip address
    - ex. capture traffic in 192.168.15.64/26 network

      - filter only allows classful networks ie 192.168.15.0
      - subnet includes top 2 bits of last octet = 01
      - byte 19 of ip header so ip[19] & 0xC0

        - final filter ip dst net 192.168.15 and ip[19] & 0xc0 = 64

- BPF Filter Examples

  - Capture all traffic to or from 1.1.1.1.

    - `tcpdump -vnni eth0 host 1.1.1.1`

  - Capture all IPv4 traffic.

    - `tcpdump -vnni eth0 ip`

  - Capture all traffic destined for 2.2.2.2.

    - `tcpdump -vnni eth0 dst host 2.2.2.2`

  - Capture all traffic between 192.168.11.110 and 192.168.111.4 only.

    - `tcpdump -vnni eth0 host 192.168.11.110 and 192.168.111.4`
    - `tcpdump -vnni eth0 host 192.168.11.110 and host 192.168.111.4`

  - Capture all traffic between 192.168.11.110 and the 192.168.111.0/24 network only.

    - `tcpdump -vnni eth0 host 192.168.11.110 and net 192.168.111.0/24`

- Exercise

  - To capture all fragmented packets, filter out packets with mf set to 0 and offset field 0

    - `ip[6] & 0x20 != 0 or ip[6:2] & 0x1fff != 0`
    - OR all equivalent to `ip[6:2] & 0x3fff != 0` -
