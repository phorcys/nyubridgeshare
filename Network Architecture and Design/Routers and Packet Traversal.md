# Routers and Packet Traversal

## Goals

- Describe the purpose and function of the Router
- Differentiate the types of routes used in LAN and VLAN architectures
- Identify the main fields in a routing table
- Identify the factors that influence how a machine chooses a route
- Create the routes needed in a routing table based on a network diagram
- Troubleshoot routing issues using system error messages and network sniffing tools

## Routers

- Device with interfaces on multiple networks
- Generally dedicated hardware
- Any device with interfaces on multiple networks can be a Router

  - usually seen with linux computers, but can also be a windows host

    - Called dual-homed or multi-homed machines

- Difference between routers and regular machines

  - Routers route packets sent by other machines.
  - regular machines solely route their own packets

- Routing Table

  - every machine that uses TCP/IP networking transmits data

    - every such machine has a routing table

  - Each entry may have the following information

    - Network Destination

      - The network number for route

    - Netmask

      - CIDR or netmask for associated network destination

    - Gateway

      - IP of next hop router to get to Destination
      - Generally not used for directly-connected networks where the router has an interface on teh network or point-to-point links such as VPNs and WANs

    - Interface

      - Locally available interface that is responsible for reaching the gateway

    - Metric

      - Integer that describes how "good" or efficient the route is

  - How things look up route

    - Determine if route is usable

      - Logical AND of destination IP and netmask of route
      - Compares result to network destination

    - Among usable routes

      - select most specific route

    - If 2 or more routes have same specificity

      - Machine prefers route with lower Metric

  - Cisco Routing Table

    - Structure of entries

      - `route-origin-code` `destination-network/CIDR` `[administrative-distance/metric]` `gateway address` `interface-name`
      - Route origin

        - One or 2 letter word to indicate how the infrormation about the route came to be

          - C - Connected
          - R - RIP
          - S - Static

            - manual input

          - etc...

      - Administrative-distance/metric

        - Reliability of a route according to the origin of that information

          - Two routes with equal specificity

            - router uses route with lower administrative distance

  - Routing Table Flags

    - [Routing table resource](https://web.archive.org/web/20160823180022/http://www.thegeekstuff.com/2012/05/route-flags/)

  - [Linux routing table](http://www.techrepublic.com/article/understand-the-basics-of-linux-routing/)

Flag | Description
:--- | :-----------------------------------------------------------------------------------------------------------
U    | Up - Route is valid
G    | Gateway - Route is to a gateway router rather than directly connected to a host
H    | Host Name - Route is to a host rather than to a network, where the destination address is a complete address
R    | Reject - Set by ARP when an entry expires
D    | Dynamic - Route added by a route redirect or RIP
M    | Modified - Route added by a route redirect
C    | Cloning - A new route is cloned from this entry
L    | Link - Link-level information such as ethernet MAC address is present
S    | Static -- Route added with the route command

- Routing protocols

  - Used for routers to communicate to get a more global picture of the network

- Things to remember about routing

  - Routers do not learn routes by inspecting traffic
  - Routes are added to routing tables

    - statically or dynamically

  - Next hop principle

    - router may be be listed as next hop but may not hhave interface on destination network

  - Default route is 0.0.0.0 netmask 0.0.0.0

    - Everything will match this route but least specific path

  - To keep tables manageable

    - create summary routes

- [NAT Resource](https://web.archive.org/web/20160824100626/https://en.wikipedia.org/wiki/Network_address_translation)

- Routing and VLANs

  - Router must have interfaces on all VLANs it is routing

    - Router on a stick
    - Problem solved with layer 3 switch

- Enable Routing in solaris 11

  - `sudo svcadm enable ipv4-forwarding`
  - verify with

    - `sudo svcs ipv4-forwarding`

- [SVCADM](https://web.archive.org/web/20150309025903/http://docs.oracle.com/cd/E19253-01/816-5166/6mbb1kqji/index.html)
- [sysctl](https://linux.die.net/man/8/sysctl)
- 
