# TCP/UDP

- Transport layer protocols

## Goals

- Identify the fields in the TCP and UDP headers
- Identify common TCP and UDP server ports
- Analyze TCP and UDP packet captures
- Identify operating systems using their TCP header features

## Fundamentals of TCP & UDP

- Transport layer protocols

  - End to end (Host to host) communication services
  - services

    - Connection-oriented data stream support
    - Reliability
    - Flow control
    - Multiplexing

- TCP

  - Complex and heavyweight protocol
  - Relies on an end-to-end connection
  - "Three-way handshake"
  - Connection-oriented

- UDP

  - Lightweight and connectionleess protocol
  - Data transmits immediately without establishing connection

- Reliability

  - TCP

    - Message acknowledgement
    - Timeout
    - Retransmission
    - multiple attemps
    - resend lost Parts
    - reliable connection
    - no missing data

  - UDP

    - unreliable
    - no verification
    - no acknowledgement
    - no Timeout
    - no Retransmission

- Ordering

  - TCP is an ordered protocol

    - messages are send in sequence
    - If received out of sequence they are buffered and reordered

  - UDP

    - Non ordered
    - not good for email, text messaging, file transfers
    - Good for VTC, Streaming

- Header length

  - TCP

    - 20 bytes

  - UDP

    - 8 bytes
    - datagram

      - Checksum

        - Complement of a 16 bit one's compliment sum of data and header information

- Multicasting

  - One to many
  - Cannot be accomplished with Connection-oriented protocols
  - UDP is most common Transport layer protocol to handle multicast

- UDP Header

  - 8 bytes
  - contains

    - Source port
    - Destination port
    - Length

      - The length field specifies the length of the data plus the length of the UDP header in bytes. This field allows for a minimum of 8 bytes (the length of the UDP header) and a maximum of 65,535 bytes.

    - Checksum

      - This is the error-checking field. The checksum is calculated by using ones' complement to the sum of the UDP data and what's known as an IP "pseudo-header."

    - Each 2 bytes

  - [UDP Header Source](http://www.faqs.org/rfcs/rfc768.html)

- TCP header

  - TCP accepts data from a data Stream, divides it into chunks and adds a header creating a tcp segment.
  - TCP segment has header and data sections
  - 10 mandatory fields and an optional extension field
  - Longer than UDP because Connection-oriented, reliable, and ordered
  - [RFC 793](http://www.faqs.org/rfcs/rfc793.html) defines TCP Header
  - components

    - Source port
    - Destination port
    - Sequence number
    - acknowledgement number
    - Offset
    - Reserved
    - Flags
    - Window
    - checksum
    - urgent pointer
    - TCP options (optional)

- ports

  - TCP and UDP allow 16 bit port space
  - Uniquely ID different applications

    - share same physical connection

  - Communication endpoint

  - IP+Port = Socket address

  - 0 - 1023

    - well known ports
    - Most OS keep track

      - Windows

        - `c:/WINDOWS/system32/drivers/etc/services`

      - UNIX

        - `/etc/services`

  - 1024 - 49151

    - Registered ports
    - Assigned to specific applications

  - 49152 - 65535

    - Ephemeral or Dynamic

  - System default Ephemeral

    - Windows before Vista

      - 1025 - 5000

    - Windows Vista +

      - IANA range

    - Linux

      - 32768 - 61000

    - Solaris

      - 32768 - 65535

    - FreeBSD

      - IANA range

- TCP Data transfer

  - Sequence number

    - ID order of bytes so data can be reconstructed
    - Must be incremented for every payload byte
    - Can be arbitrary and is better that way to prevent attacks

  - Cumulitive acknowledgement scheme

    - Sender sets seq number field to the first payload byte in the segments data field
    - receiver sends acknowledgement specifying sequence number of next byte they expect to receive
    - EX.

      - 4 byte packet
      - sender sends seq# 100, 101, 102, 103/. receiver sends ack # 104

    - If sender infers that data has been lost, it retransmits.

- Analyzing connections

  - netstat command

    - netstat -an
    - Other options

OS      | Process that has the socket open | UDP Sockets | TCP Sockets
------- | -------------------------------- | ----------- | -----------
Linux   | -p                               | -u          | -t
Windows | -o                               | -p UDP      | -p TCP
Solaris | N/A                              | -p udp      | -p tcp

- Analyzing connections

  - Socket table

    - components

      - Proto

        - Transport protocol associated

      - Local address

        - Most important for listing services
        - defines the interface the service is listening on
        - Address usage

          - 127.0.0.1:

            - Service is only accessible from the local machine (also true for any address in the loopback range: 127.0.0.0/8)

          - 0.0.0.0:

            - Service is listening on all network interfaces (this is often called "quad zip").

          - Other:

            - Service is listening only on the network interface to which the given address is assigned.

          - The port is given after the colon. It is important to know that only one service can listen on a port at any given time, regardless of the interface to which it is bound.

      - Foreign address

        - IP and port of the other end of socket connections

          - If `0.0.0.0:0` or `*.*` or `*:*` the socket is listening for new connections

      - state

        - LISTENING:

          - port is bound and can respond

        - ESTABLISHED:

          - three-way handshake complete

        - SYN_SENT:

          - often, tried to connect to closed port

        - TIME_WAIT:

          - session closed

- Exercise Links

  - [Active FTP vs Passive FTP](https://web.archive.org/web/20160915110257/http://slacksite.com/other/ftp.html)
  - [Wireshark filters](https://web.archive.org/web/20160915105757/https://www.wireshark.org/docs/wsug_html_chunked/ChWorkBuildDisplayFilterSection.html)
  - [Wireshark FTP](https://web.archive.org/web/20160915110008/https://www.wireshark.org/docs/dfref/f/ftp.html)
  - [Anonymous FTP](https://web.archive.org/web/20160915110143/https://tools.ietf.org/html/rfc1635)
  - [Raw FTP Command List](https://web.archive.org/web/20160915110408/http://www.nsftools.com/tips/RawFTP.htm)

## OS Fingerprinting

- Passive OS Fingerprinting

  - TTL gives rough estimate
  - Looking at 3 way handshake gives more granularity

    - Syn datagram is of particular interest

      - initiators preferences so gives a solid indicator of OS

    - Other 2 datagrams can also be used but are less reliable

  - p0f database and lookup tool

    - provide a catalog of operating system signatures for identificatiion based on variety of characteristics.
    - Look at custom version of database p0f.txt

  - TCP SYN datagram

    - TTL field
    - TCP Options field

      - Start at offset 54 from beginning of frame
      - Can parse with TCP header cheat sheet in resources
      - What to look at

        - Options layout (order of the options)
        - Max segment size
        - window scaling factor
        - Window size (note if multiple of max segment size or not)
        - How to read

          - Each option has 3 parts

            - Option Kind (1 byte)
            - Option Length (1 byte)
            - And Option data (variable)

          - NOP = 0x01

          - End of Options = 0x00

          - Max Segment size

            - Option Kind = 0x02
            - Option Lenght = 0x04
            - Option data

              - 2 bytes

            - Window scale

              - Option Kind 0x03
              - Option length 0x03
              - Option data 0x01

            - Selective acknowledgement permitted (selective ACK OK)(sok)

              - Option kind 0x04
              - Option length 0x02
              - No data

            - See TCP Header resource for more

      - Window size in TCP header at Ofset 14 from start of TCP header

        - Offset 48 from start of frame

- p0f format

  - Sig = ver:ittl:olen:mss:wsize,scale:olayout:quirks:pclass

    - ver = Version 4 or 6
    - ittl is TTL
    - olen = length of IP options (almost always 0)
    - mss = max segment size
    - wsize = window size
    - scale = window scale
    - olayout = olayout as written
    - quirks, pclass

      - comma-delimited properties and quirks observed in ip or tcp headers

  - Option layout is the most important field to match

    - TTL and window size/scale next

## EXAMPLE!

- `label = s:other:Nintendo:3DS`
- `sig = *:64:0:1360:32768,0:mss,nop,nop,sok:df,id+:0`
- Filter to capture a syn packet

  - TCP SYN and not a Syn-Ack
  - `tcp[13] & 0x12 = 0x02`

- TTL is at most 64

  - `ip[8] <= 64`

- window size is 32768

  - `tcp[14:2] = 32768`

- TCP Header contains 28 bytes

  - 4 for mss, 1 for each nop, 2 for sok
  - `tcp[12] & 0xf0 = 0x70`

    - Take bytes of header * 4
    - take that number and turn to hex
    - ie 28 * 4 = 112 hex is 0x70
    - use f0 because only need to look at first 4 bits in this example

- first tcp option is mss with 1360 value

  - `tcp[20:4] = 0x02040550`
  - 0x0204 standard to define mss

- last set of options nop, nop, sok

  - `tcp[24:4] = 0x01010402`

- dont fragment bit is set

  - `ip[6] & 0x40 = 0x40`

- IP ID is Non-0

  - `ip[4:2] != 0`

- Test if TCP header is greater than 20 bytes long

  - `tcp[12] & 0xf0 > 0x50`

- Test if first option byte is nop

  - `tcp[20] = 0x01`

- Have to match all so use logical AND
