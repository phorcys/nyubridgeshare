New Change

# Ethernet

## Goals

- Interpret structure of MAC
- Analyze components of 802.3 and Ethernet II framing standards
- Describe how error detection is perfromed
- use bpf to filter on Ethernet Characteristics
- Describe broadcast domains and VLANs and their impact on network segmentation

## Addressing

### ethernet address

- also known as a MAC
- First 3 bytes known as organizationally unique Identifier (OUI)
- Last 3 bytes known as Vender assigned (generally used only once)
- No duplication
- Mac address vendor search

  - <https://macvendors.com/>

- Conventions

  - Last bit of first byte determines if unicast (0) or multicast (1)

    - Unicast is single recepient
    - Multicast is multiple recepient

      - Broadcast is special type of multicast for all recepients on network

  - Second to last bit defines the "scope" of the address - Global (0 and must make sure unique across the globe) or local scope (1 unique across local network)

- Normal Operations

  - Only process frames that follow the following conventions

    1. The mac address of the Nic
    2. A multicast address the nic is configured to receive
    3. The broadcast address

- Nic Can be configured to process all frames received. AKA Promiscuous mode

- Resource <http://standards-oui.ieee.org/oui.txt>

## Framing

- Basic unit of data at the data link layer is a frame
- Standard by which a network stack adds its information to the frame

- Header

  - Destination MAC address

    - 6 Bytes

  - Source MAC address

    - 6 Bytes

  - Ether Type

    - 2 Bytes

  - 14 Bytes

- Payload

  - Data
  - 46-1500 Bytes

- CRC Checksum

  - 4 bytes

- Most popular standard is Ethernet II or DIX (DEC, Intel, Xerox)

- Frame is 18 bytes total

  - 14 bytes up front
  - 4 bytes Cyclic redundancy check at end

- IEEE 802.3

  - Used with Cisco layer 2 protocol such as STP or CDP (Cisco Discovery Protocol).
  - Identical to Ethernet II framing except instead of EtherType field it uses two bytes as a length field
  - If value of 2 bytes in "Ether type field" < 0x0600 or 1536 bytes

    - Then it is being used as a length
    - Else it is EtherType

- Maximum size of a frame is 1518 bytes

- Maximum size of data in a frame is 1500 bytes

  - Prevents a single host from holding a sharded medium with a "Jumbo frame"

- Minimum frame is 64 Bytes

  - Standardized for use with collision detection in a shared medium
  - CSMA/CD - Carrier Sense Multiple Access with Collision detection

    - Each station must be able to determine a full frame reaches the end of the line before transmitting is completed
    - Modern networks use variation that allows "jumbo frames" because no longer shared medium

  - Minimum size is generally not an issue but small things like arp require padding to reach 64 bytes

- Preamble & start frame delimiter

  - Signal sent before frame to claim medium
  - Considered part of physical layer of ethernet
  - Never seen in packet captures

- Error detection

  - When host sends frame, calculates Checksum
  - Checksum is calculated on endside.
  - If the checksums are different, frame dropped
  - Usually to check for errors with collisions or electrical interferience
  - Does not determine where error happened
  - Does not inform sender the frame received was in error, responsibility of higher layer of protocol stack
  - CRC is usually stripped before captures

    - some NICs can be configured to return CRC bytes but uncommon. Usually safe to assume CRC bytes are not present

## Ethernet sniffing

- BPF Filters

  - Ethernet shortcuts

    - ether host aabbccddeeff -

      - True if source or Destination MAC matches

    - ether src

      - True if source matches

    - ether dst

      - True if Destination matches

    - ether multicast

      - true if multicast

- Exercise

  - <https://web.archive.org/web/20160823173104/http://biot.com/capstats/bpf.html>
  - <https://web.archive.org/web/20151017091406/http://standards.ieee.org/develop/regauth/ethertype/eth.txt>

## Broadcast domains
 

- Bus topology

  - same collision domains

- Switch toplogoy

  - different collision domains
  - same broadcast domain
  - Same lan
  - large networks can have negative effects because of same broadcast domain. especially with netbios and windows
  - Improve this by segmenting networks with a router
  - Use VLAN to seagment a switch to act like several different switches. creating multiple broadcast domains

- VLANs

  - Truely separate network
  - Devices do not know they are in VLANs
  - broadcast to one VLAN only devices in that vlan receive broadcast

- Physical devices for LANS

  - Hub

    - Layer 1 device because only interpret waveform of a frame.
    - repeats frames out all interfaces.
    - same collision domain
    - same broadcast domain

  - Bridge

    - Layer 2 device
    - segment LANs into larger segments
    - examine MAC and forward to side of bridge that MAC is on
    - Learns from listening to traffic
    - Segment collision domains but maintain broadcast domains

  - Switch

    - Each port is its own collision domain
    - Layer 2 or Layer 3 depeding on if there is an attached router
    - Same broadcast domain
    - Allow VLANs to segment broadcast domains

  - router

    - Route packets between networks
    - Each interface generally a different collision domain and different broadcast domain
    - Layer 3 because it uses network layer data
