# Network Helper Protocols

## Goals

- Identify the purposes of the helper protocols

  - ARP, ICMP, and DHCP

- Describe how ARP operates

- Identify the importance of ICMP Error Types and Codes

- Describe the Ping Request/Reply Process

- Describe the DHCP IP assignment Process

- Identify the DHCP renewing process

## Address Resolution Protocol

- [ARP HEADER LINK](https://web.archive.org/web/20160824100734/http://www.tcpipguide.com/free/t_ARPMessageFormat.htm)
- Provides functionality to translate IP address to MAC address
- ARP meant to be used in single lan
- If hosts are in the same network,

  - Host A will use ARP to communicate directly with Host B

- If Hosts are not in the same network,

  - Host A will look in routing table for next hop router,
  - Use arp to determine MAC address of next hop router

- Request and reply protocol

  - Request

    - Initiating machine sends to MAC BROADCAST
    - Says I need x.x.x.x MAC address
    - Includes Source IP and MAC

  - Reply

    - Unicast direct to sender
    - Includes declaration of ownership of IP
    - Includes MAC address

- ARP Caching

  - sending arps for every segment would overflow network
  - Use caching to reduce network traffic
  - Reasons for caching

    - efficiency

      - faster resolution for commonly used addresses
      - reduces network traffic

  - Table containing sets of MAC and IP addresses

  - Static and Dynamic entries

    - Static

      - Manually added
      - permanently kept
      - accomplished with `arp -s <ip> <mac>`

    - Dynamic

      - Added by software when resolution completes
      - Temporarily kept, then removed
      - Linux/Unix - 60 seconds
      - Windows - 120 Seconds

- Arp in action

  - Assume arp cache empty
  - H1 wants to send ping to H2
  - H1 must encapsulate ping in frame

    - Needs MAC to build frame

      - Checks arp cache for MAC for H2 IP

        - Empty

          - Must use ARP to determine

  - ARP sent out to BROADCAST MAC `FFFF.FFFF.FFFF`

    - All hosts receive arp request

      - If host is not H2, host drops the arp request

  - _Unfinished_

  - Flushing Arp cache

    - Windows

      - `arp -d *`
      - `netsh interface ip delete neighbors`

    - Linux

      - `ip neigh flush all`

- Arp Cache Poisoning Exercise

  - snoop arp

    - `snoop -d e1000g0 /arp`

  - verify arp cache on solaris

    - `arp -an`

## Internet Control Message Protocol

- used to relay query messages and send error messages.

- defined by rfc 792

- Assigned protocol number 1 in IP protocol header

- Not typically used to exchange data between systems

- Not regularly employed by end user network applications except for diagnostic tools like ping and traceroute

- Structure

  - Header

    - Type

      - Defines what kind of message is being sent

    - Code

      - Identifies the reason for the message, if it is an error

    - Checksum

    - Other message specific information

- ICMP Error message

  - payload usually contains the following

    - IP header and first 8 bytes of data responsible for generating error

  - ICMP Error messages are not generated in response to

    - Another ICMP error message
    - A message with a broadcast address (either data link or network layer) as the source or Destination
    - a fragment that is not the first
    - This avoids error loops

- **SEE SUPPLEMENTAL MATERIALS DOCUMENT FOR LINK FOR TYPES AND CODES**

- Types and codes

  - Type 3

    - Destination Unreachable
    - Codes

      - 0 - Net Unreachable
      - 1 - Host Unreachable
      - 2 - Protocol Unreachable
      - 3 - Port Unreachable
      - 4 - Fragmentation Needed & Don't Fragment was set
      - 5 - Source Route Failed
      - There are more codes not listed _see supporting documents_

  - Type 8

    - Echo Request

      - Ping
      - Tests the reachability of a host
      - Tests round-trip time from host to destination
      - Payload of echo request is generally filled with ASCII characteristics

        - Pattern is different for Windows and UNIX
        - Pattern can be set

  - Type 0

    - Echo Reply
    - Response to echo request
    - TTL decremented at each router
    - client can use identifier and sequence number to determine which echo request it is associated with
    - Data must match echo request

- ICMP bpf filters

  - ICMP destination Unreachable

    - `icmp[icmptype] = icmp=unreach`

  - ICMP destination unreachable protocol Unreachable

    - `icmp[icmptype] = icmp-unreach and icmp[icmpcode] = 2`

  - ICMP echo requests to specific network

    - `icmp[icmptype] = icmp-echo and ip dst net x.x.x.x/x`

  - ICMP TTL exceeded messages where destination of discareded packet is 10.40.30.243

    - `icmp[0] = 11 and icmp[24:4] = 0x0a281ef3`
    - IP header is encapsulated in the icmp message. Need to pull out destination ip address from the IP header.
    - IP header starts at byte 8 of ICMP payload

- Traceroute

## Dynamic Host Configuration Protocol

- Application layer protocol
- Enables server to assign IP addresses to computers in network
- distributes configureation information

  - Gateway
  - Domain Name
  - Name and Time servers
  - RFC 1531

    - RFC 2131

      - Current standard

- Connectionless

  - All done over UDP

- How it works

  - Initialization Stage

    - Client has no IP address
    - DORA

      - Discover

        - Broadcast request called Discover message to 255.255.255.255 or specific subnet broadcast address
        - IP address lease request
        - Generally occurs when the machine first turns on and OS loads
        - Source IP is 0.0.0.0
        - Asks where the DHCP server is located
        - Client can request a specific IP address
        - Lists configuration parameters requested by client

      - Offer

        - Server reserves IP for client
        - Message Includes

          - IP address offered
          - Client MAC address
          - DHCP server IP address
          - Subnet mask
          - Lease duration

      - Request

        - Broadcast message
        - Accepts IP address offered
        - Clents can receive multiple offers but accepts only one
        - This message will allow other DHCP servers to withdraw offers

      - Acknowledgement

        - DHCPACK unicast packet
        - Includes

          - IP address
          - Client MAC
          - Subnet mask
          - lease duration
          - DHCP server IP

        - Completes IP Configuration

          - Client must accept negotiated parameters

        - ARP sent at the end of the DORA process to prevent address conflicts

  - Renewing process

    - Cannot use IP address after lease expires
    - Renewing state

      - 50% of lease time expired
      - Directed DHCP Request
      - DHCPACK returned from server

        - Contains new lease time and configuration parameters

    - If DHCP client cannot reach associated DHCP server

      - After 87.5% of the lease time

        - Sends DHCPREQUEST to broadcast to attempt to reach any server
        - Any DHCP server can respond with DHCPACK message

    - If client cannot reach the server

      - Client immediately stops using IP address
      - Must rebind or return to initiating state

    - DHCPNACK

      - No acknowledgement
      - Client must return to initializing state -
