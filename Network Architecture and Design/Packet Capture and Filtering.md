# Network stack

- Application layer

  - Provides user services.
  - HTTP, FTP, SMTP, etc

- Transport layer

  - Establishes basic data channel
  - establishes process-to-process connectivity
  - end to end message transfer

- Internet Layer

  - sends packets across one or more networks. AKA Network layer

- Network Access Layer

  - Data link Layer

    - pakets between interfaces of two different hosts on the same link

  - Physical Layer

    - Defines logic levels, data rate. Bit stream.

# BPF

- ether[0] & 0x__ to filter out specific bits.
- when grabbing multiple bytes. either 2 or 4 bytes
- wireshark can do more than 2 and 4 bits
- ret 0 returns 0 bytes
- ret 65535 returns 65535 bytes
- ld load a full word
- ldh load half word
- ldb load a single byte
- jeq jump if equal to
- jset calculates bitwise & of first argument and value in A register. goes to the jt line if number is non-0 otherwise goes to jf line
- return a value specified by number of bytes
- <https://web.archive.org/web/20160823173104/http://biot.com/capstats/bpf.html>
- ether pulls beginning of ethernet frame

# Wireshark

First byte is 0xff

- eth[0] == 0xff
- <https://web.archive.org/web/20160823173004/https://www.wireshark.org/docs/man-pages/wireshark-filter.html>

# Capture full frame

- tcpsdump -s0 -c 50 -i eth0 -w /tmp/out.pcap

  - captures full packet, 50 packets total, on eth0, pipes to file
