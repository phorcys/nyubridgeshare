# Switches and Frames

## Goals

- Explain the importance and functions of a Switch
- Explain the importance and functions of a switch,
- Differentiate between how switch logic relates to the source and destination MAC addresses,
- Describe basic forwarding logic used by a switch,
- Describe how a switch performs address learning,
- Explain Spanning Tree Protocol and how it affects switching,
- Describe Virtual Local Area Networks (VLANs) and Private Virtual Local Area Networks (Private VLANs) and how they're implemented, and
- Describe how frames cross a switched network.

## MAC addresses

- Three Catagories

  - Unicast

    - Address assigned to a single host
    - Unique to single host in a network
    - Often globally unique

  - Broadcast

    - Special multicast address
    - FF:FF:FF:FF:FF:FF
    - Frame intended for processing by all hosts on network

  - Multicast

    - Send traffic to subnet of network
    - Multicast addresses in destination

      - Sent to all hosts
      - Host decides to process frame or not

- Address learning

  - Switch learns MAC addresses by examinging MAC address of each frame it receives
  - Associates MAC addresses received with port it was received over

## Forwarding logic

- Destination hardware address compared to Forward MAC database
- Switch will only transmit the frame out of the interface connected to the Destination address

  - Preserves bandwidth
  - Called **Frame Filtering**

- If a hub is connected to an interface, multiple MAC addresses will be associated with a single interface

- Switch will never forward a frame out of the same interface it was received

- Three possibilities

  - If `BROADCAST` OR `MULTICAST`

    - `FLOOD ! ORIGIN`

  - If `UNICAST` AND `! ADDRESS TABLE`

    - `FLOOD ! ORIGIN`

  - If `UNICAST` AND `ADDRESS TABLE` AND `! ORIGIN`

    - `FORWARD` to port

  - If `UNICAST` AND `ADDRESS TABLE` AND `ORIGIN`

    - `DROP`

- Managing the MAC table

  - Flushing out Entries

    - Timer to flush each entry in the MAC table
    - Flushes to purge bad data and prevent table from filling
    - default time for CISCO switches is 300 seconds

  - If a switch mac table fills, it acts like a hub

    - new addresses are not learned and traffic to new addresses is flooded
    - Can be caused by an attacker with MAC Flood attack

      - attacker sends lots of frames with new source mac addresses
      - allows attacker to sniff everyones traffic

  - MAC spoofing

    - attacker sents frames with target mac address as source. switch will start forwarding all traffic destined for the target to attacker

## Loop avoidance

- Loops occur if you hook switches together on multiple ports, or have multiple switches connected to eachother
- Broadcast frames will cause switches to flood the network with broadcast frames

  - Called **Broadcast storm**

- Downside of not having loops is that if a connection is severed

  - splits network in two

- Spanning Tree Protocol

  - used to prevent Loops
  - 802.1D IEEE algorithm
  - Uses Bridge Protocol Data Unit messages with other switches
  - Guarantees only one active path between two network devices
  - How it works

    - Blocks some ports from sending frames
    - STP Convergence occurs first then
    - Puts interfaces into one of two states

      - Blocking

        - Blocks port from sending frames

      - Forwarding

        - Allows ports to send frames

## Virtual LANs

- Assigned groups of ports on a switch that function as if they are on separate switches
- Routing between VLANs requires routing. - router or layer 3 switch
- Types of VLANs

  - Static

    - manually configured port-by-port
    - typical in production networks
    - most common
    - most secure

  - Dynamic

    - Intelligent management software
    - base VLAN based on MAC, protocol, or even applications

- Switch knows which port is which VLAN

  - done in memory
  - frame not modified

- Trunking

  - Need a connection between switches for each VLAN
  - Trunking solves this allowing all VLANs to travel over it
  - Each frame needs to be tagged with the VLAN it is associated with - VLAN tagging
  - Trunk is given a native VLAN and that data does not need to be tagged
  - Often associated switch to switch

    - can also be switch to router

  - 802.1Q tagging - VLAN Tagging

    - sending switch inserts 4 byte tag (802.1Q Header)
    - located after SA and ETHER TYPE
    - first 2 bytes are always 0x8100

      - Ether type of 802.1Q

    - Last two bytes contain 4 bits of priority information and 12 bites that define the VLAN number

    - Rare to see in captured traffic

      - stripped by switch connected to host

- Private VLANs

  - Provide layer 2 isolation between ports within same broadcast Domain
  - Types

    - Promiscuous ports

      - Communicate with all interfaces on PVLAN
      - Including Community and Promiscuous

    - Isolated ports

      - Layer 2 separation from other ports on PVLAN
      - Can communicate with Promiscuous ports
      - PVLANs block all traffic to isolated ports except traffic from Promiscuous ports
      - Traffic from issolated ports is forwareded only to promiscuous ports

    - Community ports

      - Communicate among themselves and with promiscuous ports
      - Separated at layer 2 from all other interfaces in other communities or isolated ports within their PVLAN.

  - PVLANs can be used efficiently because the number of subnets is greatly reduced

- Port Security

  - Resticts access to a switch based on MAC address
  - How to enable

    - Make sure port is an access port
    - `switchport port-security` command at the interface level
    - can then set the parameters

  - Can verify port security by

    - `show port-security` or `show port-security interface` or `running-config`

  - Port security is **CISCO** specific, but most vendors have similar functionality

## Identifying Devices in Switch MAC Tables

- See MAC exercise table

  - 6.5.2

    ## VLAN tagging

- Determine if something is in a vlan with

  - `ether[12:2] = 0x8100`

- Determine if something is in a specific VLAN ie. VLAN 20

  - `ether[14] & 0x0f = 0 and ether[15] = 20` OR `ether[14:2] = 20`

- Can be solved with
- `tcpdump 'Vlan 20'`
- Switches add and remove VLAN tagging. The devices do not know they are in VLANs
