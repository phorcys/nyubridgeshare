# Boot and Logon

## Goals

- Describe the process start sequence
- Differentiate the boot process across Windows varients (XP, 2003, Vista, 2008, and Windows 7)
- Differentiate the logon process across Windows varients (XP, 2003, Vista, 2008, and Windows 7)
- Describe the secure logon process
- Utilize various tools available to examine or modify the boot configuration

## Hard Disk Layout

- GUID Partition Table (GPT)

  - Uses Unified Extensible Firmware Interface (UEFI)
  - Only 64-bit Windows OS support GPT for boot disks on UEFI
  - But not exclusive to UEFI systems

- Partitioning

  - Divides a HDD into smaller segments
  - Two types of bootable partitions

    - MBR
    - GPT

![image](Resources/Boot and Logon/HDDLayout.jpg)

- Master Boot Record

  - Contains partition table for the disk
  - Contains executable code callded Master Boot Code
  - Stored on the first sector of the HDD

    - four partitions for each disk

  - Maximum size of 2.2 TB

  - Supports 3 partition types

    - Primary
    - Extended
    - Logical drives

  - On start

    - Examines MBR to determine which partition on installed disks is marked as active
    - Active partition contains operating system startup files

- A disk cannot be both a GPT and MBR disk

  - However, all GPT disks contain a protective MBR used for legacy programs that do not understand GPT structure

- GUID Partition Table

  - Array of partition entries describing the start and end logical block access (LBA) of each partition on disk
  - LBA 0 contains a legacy protective MBR before the GUID partition table

    - Protective MBR contains one primary partition
    - Legacy software that does not know GPT reads only the protected MBR
    - MRB also protects GPT disk from legacy tools that would not recognize the disk

  - LBA 1 Contains Partition Table Header

## Generic Pre-boot process

- Generic order

  - Begins with Bios

    - PC turned on and Bios initilizes hardware

  - MBR

    - BIOS calls code stored in MBR

  - Active Partition

    - MBR loads code from bootsector

  - And finally Bootloader


    - Bootsector loads and runs bootloader from filesystem 
    
![image](Resources/Boot and Logon/Bootprocess.PNG)


- BIOS: (Basic Input/Output System)

  - Firmware that is stored in the computer's read-only memory.
  - Its purpose is to initialize and test the hardware of the system, and locate and read into memory the Master Boot Record (MBR) code.

- MBR: (Master Boot Record)

  - Located at the first sector of the hard disk, contains boot code and a partition table.

- PBS: (Partition Boot Sector)

  - Reads in the OS Loader program (NTLDR).

- Ntldr: (Windows OS Loader)

  - The Windows OS Loader loads the boot.ini file and presents the boot select menu if necessary. In the interim NTLDR loads, but does not initialize, the kernel image, HAL images, and System registry hives into memory. (NTLDR initializes the kernel later in the boot process.)

- Boot.ini

  - Contains information defining the paths from which files are loaded for all installed operating systems.

- Ntdetect.com

  - Performs hardware detection. The information collected is stored in the HARDWARE hive later in the boot process.

- Ntoskrnl.exe

  - After being initialized by NTLDR, Ntoskrnl.exe starts the process of initializing Hal.dll. This gives Hal.dll temporary control before further system initialization.

- Hal.dll: (Hardware Abstraction Layer)

  - The HAL exists between the kernel and the hardware. It performs low-level function call translation to a form the actual hardware can interpret. It also hides any hardware dependencies from the kernel.

- Smss.exe: (Session Manager)

  - The Session Manager process is responsible for finalizing the initialization of the operating system and prepares the computer for user logon. Launches the Windows subsystem and Winlogon processes.

- Winlogon.exe: (Interactive Logon Manager)

  - Coordinates all user logon tasks, to include authentication, user token creation, and creating the user's session.

- LSASS: (Local Security Authority Subsystem)

  - Performs identification and creates the user's security access token.

- Services : (Service Control Manager)

  - Responsible for loading auto-start devices drivers and services.

- Logon process Windows XP and 2003

  - Begins when CTRL+ALT+DEL is entered

    - Secure Attention Sequence


![image2](Resources/Boot and Logon/Logonprocess.PNG)


## Boot Configuration Data

- Before Vista

  - Boot order and options modified by editing boot.ini


- New boot config stored in database called Boot Configuration Database 

![image3](Resources/Boot and Logon/BCD.PNG)


- BCD Store

  - The BCD store is stored as %SYSTEMDRIVE%/boot/BCD on the active partition on BIOS systems, for example c:/boot/bcd
  - The BCD store is located on the EFI System Partition on UEFI systems
  - This hive is loaded but hidden from view in Regedit.exe within Windows
  - The store is a registry hive loaded to the following location: HKLM/BCD00000000

- BCD Object

  - Container for all BCD elements
  - Each object has a GUID

    - represents the object in the BCD hive by naming a subkey of the objects key
    - 18 predefined BCD objects with a fixed GUID.
    - Every BCD object has a type

      - Stored as DWORD
      - Most common type describes a boot environment application

        - such as instance of the windows boot loader

      - Application objects

      - Inherit objects
      - Device objects

- BCD Element

  - BCD element is a singular item of data like, a boot application name, or an operating system device.
  - Each BCD element is its own subkey.

    - The name of the subkey is a representation in hexadecimal digits of a DWORD, referred to as a datatype

- Why is this significant?

  - Boot process can be extended to support other applications
  - Theoretically Vista could be installed on any future version of windows as long as it has the same boot structure
  - With legacy windows, installing an older version would cause new version to fail on start-up

    - due to version-specific code improvements in NTLDR

- Tools

  - BCD Tools

    - Simple tasks

      - Setting a boot timeout
      - setting a default operating system
      - configuring boot options /SOS and /Safeboot

    - Repair oriented

      - Startup
      - Repair
      - Bootrec.exe

  - MSCONFIG

    - Msconfig is the preferred UI tool for managing boot settings.
    - The tool supports BCD and allows the user to enumerate all BCD objects in the system store.
    - It allows certain elements to be altered for each OS object, including debug settings, safe mode settings, and other popular startup options.

  - BCDEDIT

    - Bcdedit.exe is a command line tool that can be used to manage BCD settings.
    - Bcdedit.exe is a replacement for Bootcfg.exe.
    - You can use Bcdedit.exe to modify the Windows code which runs in the pre-operating system environment by adding, deleting, editing, and appending entries in the BCD store.
    - Bcdedit.exe is designed to work on previous operating systems and in recovery environments.
    - Bcdedit.exe is located in the %SYSTEMROOT%/system32 directory of the boot partition. You can view the BCD store manually in WinRE by loading the hive from the Boot folder.
    - See Supplemental Materials for more information.

  - BOOTREC

    - Bootrec.exe is a tool that can be used in the Windows Recovery Environment (WinRE) to troubleshoot and repair the following items in Windows Vista or Windows 7:

      - A Master Boot Record
      - A Boot Sector
      - A Boot Configuration Data (BCD) Store


- Win Vista + boot 

![image4](Resources/Boot and Logon/win7boot.PNG)


  - UEFI Firmware:

    - Contains boot loader code that is stored in the system's NVRAM. Performs CPU and chipset initialization, loads drivers, reads the contents of the Boot Configuration Database, and is also stored in NVRAM. Loads Bootmgfw.efi.

  - Hardware Detection:

    - During the boot process, the boot loader code used EFI interfaces to detect:

      - Network adapters
      - Video adapters
      - Keyboards
      - Disk controllers
      - Storage devices

  - UEFI Boot Manager:

    - Loads UEFI device drivers. Presents the Boot Selection Menu and its timeout. Loads boot application (bootmgfw.efi).

  - Winresume.efi:

    - Contains information defining the paths from which files are loaded for all installed operating systems.

  - BCD (Boot Configuration Database):

    - Performs hardware detection. The information collected is stored in the HARDWARE hive later in the boot process.

  - Bootmgfw.efi:

    - Loads Windows OS loader (Winload.efi) selected by user.

  - Winload.efi:

    - Loads the Windows OS: Loads all files need by the kernel to initialize, and sets up the kernel's execution environment.

  - Ntoskrnl.exe:

    - This is the kernel image. Performs executive subsystem initialization. Loads and initializes BOOT_ and SYSTEM_START device drivers. Launches Smss.exe.

  - Hal.dll (Hardware Abstraction Layer):

    - The HAL exists between the kernel and the hardware. It performs low-level function call translation to a form the actual hardware can interpret. It also hides any hardware dependencies from the kernel.

  - Smss.exe (Session Manager):

    - The Session Manager process is responsible for finalizing the initialization of the operating system and prepares the computer for user logon. Launches the Windows subsystem and Winlogon processes.

  - Win32k.sys:

    - Kernel-mode portion of the Windows subsystem.

  - Wininit.exe:

    - Windows initialization process used only in Session 0.

  - LSASS (Local Security Authority Subsystem):

    - Performs Identification and creates the user's security access token.

  - Winlogon.exe (Interactive Logon Manager:

    - Known as the Interactive Logon Manager, coordinates all user logon tasks to include authentication, user token creation, and creating the user's session.

  - LogonUI:

    - Presents the Interactive logon Dialog box to collect user credentials.

  - Services.exe (Service Control Manager):

    - Responsible for loading auto-start device drivers and services

## Secure Logon Process


- Secure attention sequence 

 
![image5](Resources/Boot and Logon/Securelogon.PNG)

