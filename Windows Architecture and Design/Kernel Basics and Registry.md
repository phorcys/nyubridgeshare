# Kernel Basics and Registry

## Goals

- Define the various functions of the Windows Kernel
- Describe the differences between User-mode and Kernel-mode
- Describe the structure and contents of the windows Registry
- Describe how LastKnownGood is updated
- Demonstrate the use of the Command-Line Registry editor to view, analyze, modify and crete Registy enteries
- Identify when changes to the registry are expected to take effect

## Kernel

- Control various elements of the machine
- sits between individual running programs and the hardware
- liason between user-level programs and the hardware
- Set of functions
- contained in a file written as an embedded computer program
- What it does

  - manages system resources
  - communicates between hardware and software
  - translates I/O requests between applications and I/O devices
  - Initializes device drivers on boot

- Exists in three levels

  - High-level drivers

    - File System Drivers (FSDs)

      - New Technology File System (NTFS)
      - File Allocation Table (FAT)
      - CD-ROM file system (CDFS)

    - Always depend on support from underlying lower-level drivers

  - Intermediate-level drivers

    - Include virtual disk, mirror, Windows Driver Model, or device-type-specific class and depend on support from underlying lower-level drivers.
    - Intermediate-level drivers are subdivided further as follows:

      - Function drivers control specific peripheral devices on an I/O bus
      - Filter drivers insert themselves above or below function drivers
      - Software bus drivers present a set of child devices to which still higher-level class, function, or filter drivers can attach themselves

  - Lowest-level drivers

    - Lowest-level drivers control an I/O bus to which peripheral devices are connected.
    - Lowest-level drivers do not depend on lower-level drivers.
    - Hardware bus drivers are system-supplied

      - usually control dynamically configurable I/O buses.

    - Hardware bus drivers work with the Plug and Play manager to configure and reconfigure system hardware resources for all child devices that are connected to the I/O buses that the driver controls.

      - These hardware resources include mappings for device memory and interrupt requests (IRQs).

    - Legacy drivers that directly control a physical device are lowest-level drivers.

- Kernel is typically responsible for

  - Process and Task Management

    - The kernel manages the computer's hardware and resources and allows other programs to run and use these resources. One of these resources is the CPU. This is the most central part of a computer system, responsible for running or executing programs. The kernel takes responsibility for deciding, at any time, which of the many running programs should be allocated to the processor or processors, each of which can usually run only one program at a time.

  - Memory Management

    - The kernel code is usually loaded into a protected area of memory to prevent it from being overwritten by programs or other parts of the operating system. Memory is used to store both program instructions and data. Typically, both need to be present in memory in order for a program to execute. Often, multiple programs will want access to memory, frequently demanding more memory than the computer has available. The kernel is responsible for deciding which memory each process can use, and determining what to do when not enough is available.
    - For example, if a program needs data which is not currently in RAM, the CPU signals to the kernel that this has happened, and the kernel responds by writing the contents of an inactive memory block to disk and replacing it with the data requested by the program. The program can then be resumed from the point where it was stopped.
    - Virtual addressing also allows creation of virtual partitions of memory in two disjointed areas, one being reserved for the kernel space and the other for the applications or user space. The applications are not permitted by the processor to address kernel memory, thus preventing an application from damaging the running kernel.

  - Device Management

    - To perform useful functions, processes need access to the peripherals connected to the computer, which are controlled by the kernel through device drivers. These peripherals can include the keyboard, mouse, disk drives, USB devices, printers, displays, and network adapters.
    - For example, to show the user something on the screen, an application would make a request to the kernel, which would forward the request to its display driver, which is then responsible for actually plotting the character or pixel. A kernel must maintain a list of available devices.
    - This list may be known in advance, configured by the user, or detected by the operating system at run time – normally called plug and play. In a plug and play system, the device manager first scans the different hardware buses, such as the Peripheral Component Interconnect (PCI) or Universal Serial Bus (USB), to detect installed devices, and then searches for the appropriate drivers.

- User-mode and Kernel-mode

  - User-mode

    - Less privledged processor
    - Things executed in User-mode

      - Service processes
      - User Applications
      - System Support Processes
      - Environment Subsystems
      - Subsystem DLLs

    - Code executed cannot damage integrity of operating system

  - Kernel-mode

    - Higher priviledged processor
    - Direct access to all hardware and memory
    - I/O operations and other system services run in Kernel-mode
    - Things executed in Kernel-mode

      - Executive subsystems
      - Kernel
      - Device drivers
      - Hardware Abstraction Layer
      - Windows USER and graphics display interface functions

## The Registry

- Registry data is read during

  - initial boot process

    - Boot loader reads list of boot drivers to load into memory before initializing the kernel

  - kernel boot process

    - reads settings that specify which device drivers to load
    - reads settings for how subsystems configure themselves
    - tune system performance

  - logon process

    - Explorer and other windows components read per-user preferences

  - Application startup

    - read system-wide settings as well as per-user preferece settings including menu and toolbar placement, fonts and most-recently accessed documents

- Registry structure

  - Hierarchical database that contains keys and values
  - 5 major root keys
  - Root keys contain subkeys
  - Values contain the actual data

    - are not considered organizational units
    - Contain

      - Name
      - Data Type
      - Value

- Hive

  - discreet set of keys, subkeys, and values contained in teh registry
  - Each hive is a single file in

    - `%SystemRoot%/system32/config`
    - Associated with a .LOG file

- Root Keys

  - /HKEY_CLASSES_ROOT

    - which stores file association and Component Object Model (COM) object registration information,

  - /HKEY_LOCAL_MACHINE

    - which stores system-related information,

  - /HKEY_CURRENT_USER

    - which stores data associated with the currently logged-on user,

  - /HKEY_USERS

    - which stores information about all the accounts on the machine, and

  - /HKEY_CURRENT_CONFIG

    - which stores some information about the current hardware profile.

- Really only 2 root keys

  - /HKEY_LOCAL_MACHINE
  - /HKEY_USERS

- H stands for handle

  - Keys are accessed from a known root key handle mapped to the content of a registry key that is preloaded by the kernel from a stored hive

- Linked Hives

  - Examples

    - HKLM/System/CurrentControlSet

      - Alias of ControlSet001, ControlSet002 etc

    - HKCR

      - Alias to HKLM/Software/Classes and HKU/

        <sid>/Classes</sid>

    - HKCU

      - Alias to Current logged in user profile under HKU

    - HKCC

      - alias to HKLM/System/CurrentControlSet/Hardware Profiles/Current

  - Links not copies. Easier to keep track of one file instead of two.

  - Users cannot create an alias in the registry

    - Only Microsoft can do that

- Hive size limits

  - 32 Bit system

    - 50% of physical memory or 400 MB

  - 64 bit system

    - 50% of physical memory or 1.5 GB

  - Itanium

    - 50% of physical memory or 1 GB

- HKEY LOCAL MACHINE

  - Contains all system-wide configuration subkeys

    - BCD (Vista+)
    - COMPONENTS (Vista+)
    - HARDWARE
    - SAM
    - SECUIRITY
    - SOFTWARE
    - SYSTEM

  - HKEY_LOCAL_MACHINE/SYSTEM/Select

    - Four REG_DWORD values

      - Current

        - One used to boot the system this time unless it differs from default, indicating a system change was made before the last reboot

      - Default

        - The one to use for next boot when LastKnownGood needs updating

      - Failed

        - Last one which was Current when LastKnownGood was selected at boot

      - LastKnownGood

        - Indicates the one which was known to let the system boot correctly

- Data Types

  - Binary
  - DWORD
  - STRING
  - Most Common

    - REG_SZ
    - REG_DWORD

- Registry Tools

  - Regedit

    - GUI tool
    - CMD interface as well
    - [CMD Line Switches link](http://webcache.googleusercontent.com/search?q=cache:d7W0MiGRIeQJ:www.eolsoft.com/freeware/registry_jumper/regedit_switches.htm&num=1&hl=en&gl=us&strip=1&vwsrc=0)

  - Reg.exe

  - RegFind

  - Remote registry changes

    - Remote Registry Service

      - On by default in Win NT and 2000
      - Off by default in XP +

    - Admin Credentials

  - Console Tools

    - Reg.exe

      - Add or delete data or keys
      - Import or export hives and keys
      - Load and unload keys to troubleshoot or conduct analysis
      - Compare keys for differences
      - Syntax

        - `Reg add keyname[/v Valuename][/t Type][/d Data]`
        - Ex.

          - `reg add HKLM/Software/MyCo /v Data /t REG_BINARY /d fe340ead`

- Changes made

  - Changes to User settings take effect next time user logs on
  - Changes to application settings take effect the next time the application starts
  - Changes to system settigns take effect during next system start or boot
