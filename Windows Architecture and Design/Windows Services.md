# Windows Services

## Goals

- Perform a system characterization for Windows XP/2003 based on the information presented
- Perform a system characterization for Windows Vista/7/2008 based on information presented
- Manage system services using Windows CLI tools
- Manage system services using sysinternals

## Device Drivers

- kernel-mode processes that windows uses to interact with hardware devices
- I/O manager calls functions in the device driver to get mouse, networks

  - Three possible outcomes for I/O request

    - Queue for later process
    - send to hardware port
    - send to another device driver

- Many device drivers are loaded and configured by Plug and Play

- Devices also interface with the power manager and device manager
- Power manager handles all power settings
- device manager stores configuration information for the devices
- hal.dll stores platform specific functions and hardware data
- Windows automatically configures a device so that it works properly with other devices that are installed

  - at configuration Windows assigns a unique set of system resources to each device
  - Devices drivers included on the setup CD are stored in a single cabinet file, Driver.cab
  - file is used by setup and other system components as a driver file source ![image](Resources/Windows Services/Device drivers.PNG)
  - Win32 API

    - 32-bit application programming interface for Windows
    - Initiates I/O operations through Ntdll

  - ntdll

    - function library that exposes stubs that invoke windows system calls as exported functions

  - NtReadFile

    - I/O manager system call that creates an I/O request packet and directs it to the appropriate driver

  - I/O Manager

    - Subsystem that controls and interacts with all devices on the system
    - provides routines that drivers can call to have the I/O manager insert IRPs into the associated device queue

  - IRP

    - I/O request packet sent by I/O manager to request services from a driver

  - IoCallDriver

    - Routine that sends an IRP to the driver associated with a specified device object

- Information files

  - .inf
  - searched when windows starts or new hardware is detected
  - text files
  - provide the name and locations of driver-related files and the initial settings required for new devices to work.
  - during setup, driver.cab is copied to the WINNT/Driver Cache/platform directory on local HDD

    - "platform" variable for specific value that reflects the architecture of the system

  - Device drivers are installed with no intervention if conditions are met

    - Installing the driver does not require showing a user interface.
    - The driver package contains all files needed to complete the installation.
    - The driver package is available on the system in the Driver.cab file or was previously installed.
    - The driver package is digitally signed.
    - No errors occur during installation.

- Manual installation of drivers

  - require user to be a member of administrators group
  - Windows determines driver to load with the following criteria

    - Driver ranking schemes
    - Driver search location policies
    - Windows Driver Protection
    - Windows update

  - Devices set-up and configured in the same way are grouped into a device setup class

    - ex. SCSI media changer devices are grouped into teh MediumChanger device setup class

## Windows Services

<http://www.computerstepbystep.com/windows-xp-services-(local).html#list>

- Long running applications in their own Windows sessions
- Provide a specific funtionality to the OS
- can be started manually or automatically on boot
- can be paused, restarted, or stopped
- no user interface
- Run in the security context of specific user
- configuration information stored in subkeys under registry location

  - HKEY_LOCAL_MACHINE/System/CurrentControlSet/Services

- Service applications

  - conform to the interface rules of the service Control Manager (SCM)
  - Through configuration settings services can be configured to start

    - Automatically

      - during startup
      - During login

    - manually

    - by the application

  - A driver service conforms to the device driver protocols

    - similar to service applications but do not interact with SCM

- SCM Functions

  - Service program

    - contains executable code for one or more services
    - can be configured to execute in the context of a user account from either local, primary or trusted domain

  - Service configuration program

    - queries or modifies the services database

  - Service Control Program

    - SCP
    - starts and controls services

- Service start

  - HKEY_LOCAL_MACHINE/System/CurrentControlSet/Services[Service name]

    - each service is assigned a start value
    - Five values

      - 0x0

        - BOOT_START
        - Service is started at boot by ntldr or winload.exe

      - 0x1

        - SYSTEM_START
        - service is started when the system loads by ntoskrnl

      - 0x2

        - AUTO_START
        - Service is started automatically by SCM

      - 0x3

        - DEMAND_START
        - Service is started manually on demand

      - 0x4

        - DISABLED
        - Service is disabled

- Load Order

  - Determined by

    - Load ordering group list(GroupOrderList)
    - Order of services within a group (ServiceGroupOrder)
    - Dependencies listed under each service
    - GroupOrderList and ServiceGroupOrder located at HKEY_LOCAL_MACHINE/System/CurrentControlSet/Control

- Windows Service Hardening in XP

  - WSH
  - Special accounts

    - Local System

      - SID S-1-5-18
      - extensive local privledges acts as the computer on the network

    - Local Service

      - SID S-1-5-19
      - minimum local privileges and has anonymous creds on network

    - Network Service

      - Sid S-1-5-20
      - minimum local privileges
      - Acts as network computer

- WSH in Vista +

  - Services targets of malware
  - Restricts critical windows services by ensuring that the services run under least privileged accounts necessary
  - Windows service hardening
  - service hardening

    - specific to vista +

      - service resource isolation
      - least privilege services
      - restricted network access
      - session 0
      - kernel mode code signing (KMCS)

  - service resource isolation

  - least privilege services
  - restricted network access
  - session 0 isolation

- CLI tools for Windows Services

  - Services controller
    - get key_name
      - `sc //computer getkeyname "long name"`
    - Find dependents of service
      - `sc enumdepend service`
  - net (start/stop)
  - Drivers
  - Driverquery
  - sigcheck.exe
  - signtool.exe
