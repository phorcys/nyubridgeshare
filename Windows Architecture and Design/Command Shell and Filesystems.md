# Command Shell and File Systems

## Goals

- Demonstrate basic familiarization with the Command-Line Interface (CLI),
- Identify environmental variables,
- Demonstrate basic scripting concepts,
- Identify the main components of the NTFS File System,
- Utilize Windows Management Instrumentation Command-Line (WMIC) commands to obtain and evaluate responses, and
- Utilize PsExec commands to obtain and evaluate responses.

## Windows Command Shell

- Simple low overhead interface
- Character based applications and utilities
- Provides user with direct communication with OS
- Can be used locally or remotely for administration, monitoring, or troubleshooting
- Different varieties taught

  - cmd.exe
  - powershell
  - WMIC

## CMD.EXE

- Based on Command.com
- Loads applications
- Directs flow of information between applications
- Translates user input into a form that the OS understands
- Operates in Batch mode
- Supports long file names
- Has a built-in buffer to recall past commands
- Has two categories of native commands built-in by Microsoft

  - Internal commands

    - Exist in the shell
    - Ex.

      - copy
      - move
      - `dir`
      - `set`
      - `date`

  - Standard external commands

    - Own executible file

      - usually found in %systemroot%/system32

- Uses `Help` or `/?` to show available commands

- Located at the following locations

Shell        | System  | Location
:----------- | :----- | :--------------------
32-Bit Shell | 32-Bit | %systemroot%/system32
32-Bit Shell | 64-Bit | %systemroot%/syswow64
64-Bit Shell | 64-Bit | %systemroot%/system32

- System root is Environment Variable
Fpw
  - C:/Windows/

- Command shell operations

  - Customize appearance
  - View command history
  - Add macro functionality
  - Activate auto-complete
  - Increase command history buffer size

- Interactive mode

  - Enter commands manually

- Batch mode

  - Commands are read and executed one by one
  - typically from a script file

## Powershell

- Full featured command shell
- Uses Cmdlets (Built in commands)

  - Not standalone executables
  - Verb-Noun form
  - Complete list

    - `help *-*`

- Built-in programming features

- Standard command-line utilities

- Used to manage enteroprise-wide servers and workstations
- Included with Windows 7 and Server 2008 R2
- Available for download on XP and later versions

## WMIC

- Simplifies the use of Windows Management Instrumentation
- Enables administrators to enter commands to the interface without native query language knowledge
- Compatible with existing shells and utility commands
- Supported by Windows XP or Server 2003 and newer systems
- WMIC shell opens to root/cli -

## Environment Variables

- Strings that are used by windows to communicate information about the location of system files, folderes, and programs
- Located at HKLM/SYSTEM/CurrentControlSet/Control/Session Manager/Environment
- Commands

  - set

    - displays current environment variables

  - setx

    - creates or modifies env variables in the user or system environment

  - echo %systemroot%

  - echo %homepath%

## Special Characters

- Redirects

  - <>
  - Can send output to file or device
  - Can receive input from file or device
  - > >

    > - Appends

- Pipes

  - |
  - Allows for multiple redirects
  - Command 1 | command 2

    - output of command 1 is input of command 2

- Chaining & Grouping

  - Separates command on each line

Character | Definition
:-------- | :------------------------------------------------------------------------------------------
&         | separate multiple commands
&&        | Used to run command following symbol only if command preceding symbol is successful
pipe pipe (unable to use pipe symbol)       | Used to run the command following the symbol only if the command preceding the symbol fails
()        | Can be used to group sets of commands for conditional execution based on success or failure

- Escape Character

  - allows the command symbol to be treated as ordinary text
  - ^

- Wildcard

  - Replace or represent 0 or more characters

    - *

      - Represents 0 or more characters

    - ?

      - Represents 0 or 1 character

## NTFS

- Key components

  - Sector

    - Sectors are hardware-addressable blocks on a storage medium. They are typically 512 bytes and are defined in the Partition Boot Sector.

  - Cluster

    - Clusters are the addressable blocks that many file system formats use. They are always a multiple of the sector size, typically 4096 bytes, (or 8 sectors), in Windows and are defined in the Partition Boot Sector

  - Logical Cluster Number

    - The Logical Cluster Number is the cluster address relative to the storage media.

  - Virtual Cluster Number

    - The Virtual Cluster Number is the cluster address relative to the start of the file.

  - File System Format

    - The file system format defines the way that file data is stored on storage media.

  - Metadata

    - Metadata is the data stored on the volume that defines how the file system is implemented within the volume.

- Adv. Features

  - Multiple Data Streams

    - With multiple data streams, each unit of information associated with a file is implemented as a file attribute. Each attribute consists of a single stream. This implementation makes it easy to add more attributes

  - Unicode-Based Names

    - Unicode-based naming is a 16-bit character coding scheme that allows each character in each of the world languages to be uniquely represented. This makes it easy to move data from one country to another. Each complete filename path can be up to 255 characters in length.

  - Dynamic Bad Cluster Remapping

    - This feature is enabled when a bad cluster is identified and the data is remapped to a new cluster. The process is as follows: the NTFS sends a warning that the cluster is bad, and then a good copy of the sector's data is retrieved. A new cluster is allocated, and the data is copied to the new cluster. The bad cluster is flagged and no longer used.

  - Per User Volume Quotas

    - This feature allows for per-user quote specification of quota enforcement, which is useful for usage tracking and tracking when a user reaches warning and threshold limits.

  - Encryption

    - This process is transparent to the user and is performed using the Encrypting File System (EFS). Encryption is accomplished using private/public key pairs.

  - Read-only Support

    - This feature was introduced with Windows XP and allows mounted volumes to be read-only.

  - Defragmentation

    - This feature is enabled when files occupy non-contiguous space on the hard disk. The NTFS has tools to make the files contiguous.

  - Robust File permissions

    - Discussed in a later module

- Master File table

  - "Heart of NTFS"
  - Every file and directory has at least one entry in the MFT
  - Contains attributes that define and describe the MFT and its subcomponents
  - Entries are likely 1KB in size

- Metadata files

  - Administrative data
  - First 16 entries for standard system metadata files
  - Entries 17-23 are overflow when reserved are not enough
  - First entry allocated to a user file or directory is entry 24
  - First 12 MFT file entries

    - $MFT (Entry 0)

      - Entry for the MFT

    - $MFTMirr (Entry 1)

      - Mirror of the entry for the MFT

    - $LogFile (Entry 2)

      - Journel that records metadata transactions

    - $Volume (Entry 3)

      - Contains volume information such as label, identifier, and version

    - $AttrDef (Entry 4)

      - Contains attribute information such as identifier values, names, and sizes

    - / (Entry 5)

      - Root directory of the file system

    - $Bitmap (Entry 6)

      - Contains the allocation status of each cluster in the file system.
      - If the bit is set 1 it is allocated

    - $Boot (Entry 7)

      - Contains the boot sector and boot code for the file system
      - Provides basic information such as

        - the size of each cluster
        - number of sectors in file system
        - starting cluster of the MFT
        - Size of each MFT entry
        - Serial number for the file system

    - $BadClus (Entry 8)

      - Contains clusters that have bad sectors

    - $Secure (Entry 9)

      - Contains information about security and access control for the files (Win 2000 and XP)

    - $Upcase (Entry 10)

      - Contains the uppercase version of every unicode character

    - $Extend (Entry 11)

      - COntains files for optimal extensions (NOTE: Microsoft does not typically place the files in this directory into the reserved MFT entries)

- MFT Entry Layout

  - No strict layout requirements
  - MFT Zone is a collection of clusters not used to store file or directory content

    - Created to be as small as possible
    - Only expands when needed for additional entries
    - Can become easily fragmented

  - $BOOT is always first cluster on windows file systems

- MFT Entry Standard Attributes

  - $Attribute

    - $STANDARD_INFORMATION

      - Type ID of 16
      - 72 bytes in windows xp
      - Always first attribute in MFT entry
      - Contains

        - Ownership
        - security
        - quota
        - timestamp

          - Creation

            - Time the file was created

          - Modified

            - Time the contents of $DATA or $INDEX were modified

          - MFT Modified

            - Metadata time

          - Accessed

            - last accessed time

    - $FILE_NAME

      - Type ID of 48
      - 66 Bytes plus file name
      - Occurs at least once per file MFT entry
      - Contains the same temporal stamps as $STANDARD_INFORMATION

        - Correspond to when file created, moved, renamed

    - $DATA

      - Type ID of 128
      - Varible size
      - Every file has this attribute and contains file content

        - Resident

          - File 700 bytes or less

        - Non-resident

          - File greater than 700 bytes

      - May contain more than one $DATA attribute and these additional attributes are called alternate data streams
      - Original $DATA attribute does not have a name associated with it

        - Additional $DATA attributes must be named

- Opportunistic Locks
  - "OPLOCK"
  - Client request opportunistic locks
  - Request from a client to the server
    - Server grants request based on factors
  - Coordinate data caching a coherency between clients and servers and among multiple clients
- Windows File location
  - C:/
    - Program Files
      - Contains installed programs
    - Users
      - contains user profiles (Vista +)
    - Documents and Settigns
      - Contanes user profiles (user hives)(XP)
    - Program Files(x86)
      - contains installed 32-bit programs on 64-bit architectures
    - Windows
      - Prefetch
        - XP +
        - Stores pathname and mapping of last time application was run.
      - SoftwareDistribution
        - contains windows downloaded updates
      - $NT*
        - Location of replaced files after a system update or service pack
        - designed so a user can roll back to previous versions
      - system32/config
        - contains registry files
      - system32/dllcache
        - Contains backed up critical system files
      - drivers/etc
        - contains system (tcp/ip) network files
      - system32/Repair
        - contains the backup or off-line copy of registry files

## Windows Management Instrumentation (WMI)
- Defines a set of environment-independent specifications
- Allow management information to be shared between management applications
- Access through WMIC
- Installed on everything Windows Millennium +
- MS inplementation of Web Based Enterprise Management (WBEM)
  - Built on Common Information Model (CIM)
- WMIC Commmands
  - Based on Aliases
  - Commands are composed of
  - ALIASes
    - Friendly renaming of class property or method
    - makes WMI easier to use and read
    - Can determine what aliases are available with `/?`
    - Can determine the aliases for a speciffic class using `<className> /?`
  - VERBs
    - To use a verb
      - Enter alias followed by the verb
      - if alias does not support the verb you will get a message
        - "Provider is not capable of the attempted operation"
  - SWITCHes
    - WMIC Option you can set
    - Set globally or optionally
  - Example
    - Get processes running on system
      - `Process` alias
        - Shows all processes running on system
      - `process where (Description="explorer.exe")`
        - shows specific process
        - `path win32_process.Description="explorer.exe"`
          - same but without alias
      - `process get name, handle, pagefaults`
        - Shows specific process properties
        - `class win32_process get name, handle, pagefaults`
          - same as above but without alias
    - Remote system
      - `/node`
      - `/user`
      - `/password`
      - Switches can be used

## PSExec
- Lighweight telnet utility
- Most powerful tools
  - Launching interactive command-prompts on remote systems
  - remote-enabling tools like 'ipconfig' that otherwise do not have the ability to show information about remote systems
- basic use
  - `psexec //remote-computer -u username -p password command-to-be-executed`
    - remove -p to allow promp and hidden response
- Commands
  - Display IP interface configuration
    - `psexec //remote.widget.com -u administrator ipconfig /all`
  - open interactive shell
    - `psexec //remote.widget.com -u administrator cmd`
  - `-accepteula` option key if you do not have the ability to accept pop-up boxes
  -
