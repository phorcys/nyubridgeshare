# Windows Processes

## Goals

- Enumerate processes useing CLI tools
- Enumerate processes useing Sysinternals tools
- Enumerate Dynamic Link Libraries using CLI tool
- Explain the Portable Executable Format including datea structures and PE file's executable code

## What is a process

- Sequence of instructions to perform a specific task.
- Instructions are executed

  - computer behaves in a predetermined manner

- When a program is launched, it is represented as a process

- Process is a program in a state of execution

- Program may have two or more processes running

## Process overview

- Process is defined as a container for set of thread resources
- Each process has its own assigned virtual address space

  - Independent to the virtual address space assigned to other processes
  - processes' code and data is stored and protected from other processes
  - processes' threads and allocated resources are also stored here

- Process includes the following components

  - Code
  - Global Variables
  - Process Heap
  - Data
  - Process resources
  - Open Handles
  - Heaps
  - Environmental Information
  - Thread 1

    - Thread Local Storage
    - Stack

  - Thread 2

    - Thread Local Storage
    - Stack

- Process Binaries

  - Binary is a file that a computer reads or interprets
  - not text file

    - not considered human-readable

- Process memory

  - Virtual address space allocated to a process at the time the process is created
  - Total physical memory supported by Windows ranges from 2GB to 2 TB

    - Virtual address space may be greater or less than total physical memory available to system

  - Physical memory and virtual address space organized into units of memory called pages

    - Physical memory shared by all running processes
    - paging is used to move page files to disk when they are not in use

  - Virtual address space consists of several categories

    - Working Set

      - amount of memory physically mapped to a process at a given time

    - Paged pool

      - memory in paged pool can be transferred to the paging file on the disk
      - Referred to as paged out when not being used

    - Non-paged pool

      - memory in the non-paged pool cannot be transferred to the paging file on the disk as long as its corresponding objects are allocated

    - Pagefile

      - pagefile monitors memory usage and how much memory is set aside for the process in the system paging file

- Interprocess Communications

  - Allow applications to communicate and exchange data
  - Either client or server

    - client

      - application or process that requests a service from another application or process

    - server

      - application or processs that responds to requests made by clients

  - IPC Mechanisms

    - Pipes

      - Named pipe

        - connection oriented duplex communication

      - Anonymous pipe

        - only implemented in local computer

    - Mailslots

      - Unreliable connectionless one way communication
      - Psudo-file

    - RPC

      - Remote procedure call
      - call functions remotely
      - refers specifically to the application
      - remote function executes on the local or remote computer but is external to calling application

    - Winows Sockets

      - Protocol-dependant interface
      - takes advantage of capabilities of underlying protocols
      - capable of supporting current and emerging networking capabilities

    - Clipboard

      - Copy-pasta operations between applications
      - central repository for data to be shared

- Data Execution Prevention (DEP)

  - Uses hardware and software tech to prevent malicious code from executing
  - marks memory pages and other areas of memory as executable or non-executable
  - memory locations and pages typically contain datastructures such as default heap pages, various stack pages, and memory pool pages

    - Code does not typically execute from those locations

  - Code cannot execute from memory marked as non-executable

  - Useful against Buffer overflows or injects

  - Enabled by default for core OS binaries and applications explicitly configured to use it

- Address space layout randomization

  - Older version

    - core processes were loaded into memory in predictable locations when the system started up. Because of this, it was easy to exploit system

  - ASLR

    - Vista +
    - Randomizes memory locations used by system processes and other applications
    - Forces attacker to guess correct memory locations used by any given process

  - effective against buffer overflow and many Zero-Day attacks

- Registy redirecton

  - registry redirector

    - performs registry redirection for all registry calles made by a 32-bit application
    - redirects to mapped physical location under Wow6432Node subkey

      - HKLM/Software -> HKLM/Software/Wow6432Node

  - system stores 32 and 64 bit logical views of registry

- Process enumeration CLI tools

  - PsList

    - sysinternals
    - lists running processes
    - used locally or remote computer
    - 32-64 bit

  - Tasklist

    - Native windows utility
    - lists current running processes
    - 32-64 bit

  - PsKill

    - Sysinternals
    - allows process termination
    - 32-64 bit

  - TaskKill

    - native tool
    - process termination
    - 32-64 bit

  - pssuspend

    - sysinternals
    - capability to suspend process or resume suspended process
    - 32-64 bit

  - handle

    - sysinternals
    - provides list of all open file references on system for each running process
    - 32-64 bit

  - listdlls

    - sysinternals
    - list of dlls loaded by running process
    - 32-64 bit

  - pmon

    - process resource monitor
    - windows resource kit
    - 32 bit only
    - measures CPU and memory use of processes

- Integrity levels

  - Component store uses NTFS hard links between itself and other windowes directories
  - Integrity levels

    - Extends security by restricting access between objects running under same account
    - enforces security using integrity levels

      - MAC - Mandatory access control

    - A lower-level subject cannot modify a higher-level object

    - Uses access control lists to maintain integrity levels

    - different integrity levels include

      - Untrusted
      - Low
      - Medium
      - High
      - System

  - Major parts to the mechanism

    - Predefined integrity levels and their representation.
    - Integrity policies that restrict access permissions.
    - Integrity level assigned to the security access token.
    - Mandatory label access control entry.
    - Mandatory labels assigned to objects.
    - Integrity restrictions within the AccessCheck and kernel-mode SeAccessCheck APIs

  - Mandatory access token policies

    - No write up

      - default policy assigned to all access tokens
      - policy restricts write access by this subject to any object at a higher integrity level

    - New process min

      - controls the behavior of assigning the integrity level to child processes
      - child process with be minimum integrity level of parent or access token or object integrity level of executable file for the new process
      - set by default in all access tokens

  - Mandatory lable policies

    - no write up

      - same as access token policies

## Threads Heaps and Stacks

- Thread

  - Basic unit of CPU utilization
  - Smallest sequence of programmed instructions that can be managed independently
  - component of a process that is actually scheduled for execution on a processor
  - normally consists of a program counter, a stack, a set of registers, and a thread ID

- Heap

  - Area of pre-reserved memory that a program or process uses to store data in varying amounts
  - These amounts will not be known until runtime

- Stack

  - Data structure used to store a collection of objects
  - LIFO
  - Two principle operations that can be performed on the objects in the stack

    - Push
    - Pop

- Security context and impersonation

  - Context of a process describes the resources and environment of a process
  - Security context of a process describes the privileges or permissions associated with a process

    - Threads of a process execute in the security context of its parent process

  - Impersonation is the ability of a thread to execute in a security context that is different from that of its parent process

    - generally implemented to meet the securitty requirements of client/server applications

  - In client/server applications, a service running in the security context of the client application represents the client application to the server application.

- Thread scheduling

  - Priority-driven, preemptive scheduling system

    - At least one of the highest priority threads, that are ready to be dispached to a processer, will be running on a processor

  - no single scheduler module, mechanism or routine.

  - Code exists throughout the kernel where scheduling-related events occur

    - known as kernel dispatcher

- Thread execution

  - executes in predertermined amount of time

    - called a quantum
    - Quantum is the length of time a thread is allowed to run on a processor before being preempted

      - allows another thread of the same priority to run

  - Thread may not be allowed to run to the end of its quantum because another thread of higher priority becomes ready to execute

    - current thread is preempted and removed from processor for the higher priority thread

- Process and thread priorities

  - Priorities determine which threads to run
  - 32 priority levels

    - 0 lowest
    - 31 highest
    - 16-31 real time
    - 0-15 variable level
    - 0 reserved for zero page thread

  - Scheduling priorities

    - Windows API and Windows Kernel
    - Windows API organizes threads according to priority class to which its parent is assigned

      - REALTIME_PRIORITY_CLASS
      - HIGH_PRIORITY_CLASS
      - ABOVE_NORMAL_PRIORITY_CLASS
      - NORMAL_PRIORITY_CLASS
      - BELOW_NORMAL_PRIORITY_CLASS
      - IDLE_PRIORITY_CLASS

    - Within those classes, Ranked High to low

      - THREAD_PRIORITY_TIME_CRITICAL
      - THREAD_PRIORITY_HIGHEST
      - THREAD_PRIORITY_ABOVE_NORMAL
      - THREAD_PRIORITY_NORMAL
      - THREAD_PRIORITY_BELOW_NORMAL
      - THREAD_PRIORITY_LOWEST
      - THREAD_PRIORITY_IDLE

  - Base priority defined by

    - Process priority class combined with Thread priority

- Windows Side by Side

  - Windows component store directory (C:/Windows/winsxs) is used during servicing operations

    - windows update, service pack, hotfix installations and more
    - Contains all files required for windows installation and all updates are held while updates are installed
    - Grows over time

- DLL

  - Library of functions taht can be used by an application
  - Each DLL represents a specific function
  - DLLs can be used by more than one program at the same time
  - Allow a program to be modular
  - Easier to apply updates
  - Variety of extensions

    - DLL

      - Stands for Dynamic Link Library
      - Binary files shared among running programs
      - Allow programs to interact with OS
      - Each DLL writed to perform a specific function

    - OCX

      - Object Linking and Embedding Control Extension

        - Active X controls

      - Interface behaviors triggered by users of programs

      - Scroll bar movements and window resizing

    - DRV

      - Associated with legacy device driver files
      - software that interacts with a specific device or other specific software
      - device driver files have specific knowledge about associated device
      - Allows programs to communicate with a device

  - Advantages

    - use fewer resources
    - reduce code duplication

      - share code with more than one process

  - Disadvantage

    - DLL Hijacking

      - DLL preloading attack
      - binary planting attack
      - Exploits hwo applications locate DLLs

        - Used against programs that do not use full path when loading DLL

- Portable Executable files

  - File format for

    - executables
    - Object codes
    - DLLs
    - Font Files

  - Data structure that encapsulates teh information necessary for the windows OS loader to manage the wrapped executable code

  - important to know that a PE file looks similar on disk and after it has been loaded into system memory

    - prefabricated
    - reduces amount of work of loader program

  - Header

    - DOS Header

      - first component
      - first 64 bytes
      - in case program is running from DOS
      - Contains signature and offset to PE header

    - PE Header

      - Defines what the rest of the file looks like
      - contains essential info used by the loader
      - Not at beginning of file.
      - Several hundred bytes into the file
      - Contains

        - Signature field
        - processor information
        - processor type
        - number of sections
        - relative offset of the section table
        - characteristics of the file

          - file type or extension

    - Optional Header

      - Contains most meaningful info

        - initial stack size
        - program entry point
        - preferred base address
        - os version
        - section assignment info
        - etc.

    - Data Directories

      - final 128 bytes of optional header
      - data array of 16 IMAGE_DATA_DIRECTORY structures

        - see list in transcript

    - Section Table

      - array of data structures
      - each structure contains info about one section in the PE file
      - Includes

        - section name
        - virtual size
        - virtual address size of raw data
        - pointer to raw data
        - characteristics
        - order presented in section table

  - PE File section

    - Follows header
    - .text
    - .bss
    - .rdata
    - .data
    - .rscs
    - .edata
    - .idata
    - imports/exports

- Common system processes

  - Idle

    - not a real process
    - Counts cycles where the processor(s) are not doing anything

  - System

    - Represents kernel process (ntoskrnl.exe)

  - SMSS

    - SMSS process
    - manages sessions on the system
    - starts wininit.exe and winlogon.exe

  - CSRSS

    - User-mode portion of windows subsystem

  - Wininit

    - session 0 initilization process
    - runs in session 0 only (OS session)
    - Starts any autostart devices and drivers applicable to session 0 and launches lsass.exe

  - winlogon

    - interactive logon manager
    - instance runs in all sessions above 0
    - coordinate all logon activites and security-related functions within its session and launches services.exe

  - services

    - SCM is responsible for starting all auto-start devices and drivers applicable to its session

  - lsass

    - runs in session 0
    - performs all logon functions including user authentication
    - verifies the users access and creates user's access token

  - lsm

    - runs in session 0 only
    - manage all connections related to the terminal server
    - monitor all other sessions
    - if it detects unstable state above session 0

      - signals smss to terminate the session

  - Svchost.exe

    - hosts or contains other individual services that Windows uses to perform various functions
    - Windows defender uses a service that is hosted by an svchost.exe process
    - can be multiple instances of svchost.exe running on your computer.
    - One instance of svchost might host a single service for a program and another might host several services
    - each can contain a group of services Groups (in registry location HKLM/Software/Microsoft/Windows NT/CurrentVersion/Svchost)
    - can use task manager to view which services are running under each instance of svchost.exe

  - Server mode process

    - process that provides a service to the local computer or to the network
    - server mode services can be used to determine

      - role of the computer
      - computer is a server or workstation
      - what services it provides

    - Run regardless of whether a user is logged on
    - Examples

      - DNS
      - MSDTC
      - IDS
      - Endpoint security
      - IIS services
      - File replication services on Domain Controllers

  - User-mode processes

    - directly related to user logged on
    - represent applications and services launced either by user or as a result of the user logging on
    - programs that usually only run when a user is logged on
    - Closed when a user logs off
    - Example

      - Explorer.exe
      - Application processes

  - Protected mode internet explorer (PMIE)

    - IE 7+ in vista +
    - Feature that makes it more difficult to install malicious software or destroy data
    - restricts user priviledges
    - Turned on by default
    - warns user when trying to install or run software on their system
    - runs in low integrity level
    - if compromised, should limit damage
    - cannot affect/create any object at higher levels
