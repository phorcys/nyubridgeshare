# Networking and Name Resolution

- Describe common Windows networking services and ports
- demonstrate ability to use Windows networking commands
- identify common commands when using the Netstat tool
- Demonstrate ability to map listening ports to startup location
- Explain DNS and NetBIOS name resolution
- Explain the difference between hostnames and NetBIOS names

## Windows naming services
