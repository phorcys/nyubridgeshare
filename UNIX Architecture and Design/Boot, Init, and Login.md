# Boot, Init, and Login

## Goals

- Describe the boot process from power on to the login prompt
- determine what processes start at boot-time
- Trace `init` through the post-kernel boot process
- use tools to eamine or modify boot configuration
- Evaluate boot configuration files in order to parse files effectively

## commands to know

- chkconfig
- init
- invoke-rc.d
- runlevel
- service
- svcadm
- svc.startd
- systemctl
- systemd
- update-rc.d
- who

## Kernel initialization

- during bootloader

  - kernel loader

    - establishes memory management
    - interogates the CPU
    - passes execution to kernel code

- Location of kernel image depends on vendor

  - solaris

    - /kernel
    - /platform

  - Linux

    - /boot/vmlinuz

      ## Kernel Threads

- kernel provides housekeeping operations

- Swapper

  - kernel thread in solaris
  - not a kernel thread in linux
  - sched
  - share processor among all processes
  - kernel threads come from swapper

- kthredd

  - kernel thread
  - base to create all other kernel threads
  - above linux 2.6.32

![image](Resources/Boot, Init, and Login/Management tools.PNG)

![image](Resources/Boot, Init, and Login/SMF vs SysV v systemd.PNG)
