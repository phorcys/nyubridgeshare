# UNIX Filesystems
## Goals
- Describe how files and directories are stored and referenced on a partition
- explain the general format of a filesystem (on a partition)
- Explain the concepts of blocks and slack space
- Describe the UNIX logical directory structure in relation to a storage medium
- Summarize the relationship between devices, partitions, volumes
- Perform the steps to mount a partition
- Identify common root-level directories, subdirectories, and pseudo-directories in UNIX
- Describe objects on a UNIX filesystem
- Determine command usage and systax for commands related to filesystems
## Commands to learn
- dd
- diskinfo
- df
- du
- fdisk
- file
- fsck
- fuser
- ln
- mkfs
- mount
- quota
- stat
- strings
- umount
- 
