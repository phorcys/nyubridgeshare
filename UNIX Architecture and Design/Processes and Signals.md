# Processes and Signals

## Goals

- Explain how UNIX operating systems represent processes
- explain the purpose of signals in unix operating systems
- use a process list to analyze a unix system

## Commands to learn

- UNIX

  - chmod
  - chown
  - fuser
  - kill
  - pgrep
  - pkill
  - pmap
  - ps
  - top

- Solaris

  - pargs
  - pcred
  - pfiles

    - get information about file discriptor table

  - pldd

  - preap
  - prstat
  - prun
  - psig
  - pstop
  - ptime
  - ptree
  - pwdx

- Linux

  - atop
  - htop
  - lsof

    - similar to pfiles

  - pstree

## Process structure

- Process numbers

  - Process Identifier (PID)

    - PID max is kernel tunable value
    - Solaris

      - /etc/system

        - reboot

    - Linux

      - /etc/sysctl.conf

        - reboot

      - sysctl cmd

      - /proc/sys/kerne/pid_max

    - Absolute max

      - 32768 on 32 bit linux
      - 4194304 on 64 bit linux
      - 30000 on solaris
      - 99999 on bsd including osx

  - PPID

  - Process group identification (PGID)

    - signal sent to group, sent to all processes in group

- Credentials

  - Real user identifier (RUID)

    - match UID of user who started process

  - Real Group Identifier (RGID)

    - match GID of user who started process

  - Effective User Identifier (EUID)

    - usually match UID

      - Except when Setuid bit set

  - Effective Group Identifier (EGID)

    - Usually match GID

      - Except with setgid bit set

  - Init can only be sent signals for which it has a handler installed

- Process states

## Signals

![image](Resources/Processes and Signals/Common signals.PNG)

## Process list and PS command
- PS command
  - information retrieved from process list
  - via the /proc directory

![PS list variables](Resources/Processes and Signals/PS output.PNG)
