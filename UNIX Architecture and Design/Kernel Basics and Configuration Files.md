# Kernel Basics and Configuration Files
## Goals
- Differentiate between UNIX and UNIX-like Operating systems
- Define the Kernel
- Interpret `strace` and `truss` utility outputs to determine system calls used by programs
- Describe UNIX configuration mechanisms, including system and kernel configuration files
- Explain how to load and unload kernel modules
- Sys calls to learn
  - chmod()
  - chown()
  - close()
  - execve()
  - free()
  - malloc()
  - open()
  - read()
  - recv()
  - send()
  - socket()
  - write()
- Unix commands to learn
  - chmod
  - chown
  - dmesg
  - insmod
  - lsmod
  - make
  - modprobe
  - pmap
  - strace
  - sysctl
  - truss
  - uname
## Evolution of Linux
## Utilities
- Strace and Truss
  - Determine exactly what system calls are executed in extreme details
## Kernel Configuration files
- Kernel parameters can be altered during runtime
![img](Resources/Kernel Basics and Configuration Files/Kernel parameters that can be altered during runtime.PNG)
