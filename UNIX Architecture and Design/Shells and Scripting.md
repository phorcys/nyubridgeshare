# Shells and Scripting

## Goals

- Explain the purpose of a shell in a UNIX operating system
- Identify commonly used shells and their unique core characteristics -
- Identify features and functions of the shell environment
- Recognize the use of patterns in writing commands for different shells
- Demonstrate basic familiarity with shell script programming

## Shells

- Bourne shell
- C shell
- Korn Shell
- Tenex C Shell
- Bourne Again Shell
- Z Shell

## Shell Expansion
## Shell modes
- login shells have
  - - in the beginning
  - -l or --login
- interactive shells
  - have to look at children
  - or may have -i
- 
