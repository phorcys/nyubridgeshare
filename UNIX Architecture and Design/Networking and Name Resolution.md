# Networking and Name Resolution

## Goals

- Describe the local name resolution process on a UNIX host
- Analyze the output of network-related utilities
- List and identify ports on the provided system
- describe the purpose of name resolution
- describe network file systems
- analyze entries within a socket table
- create a bash backdoor unsing xinetd on the provided system

## Commands to know

- Unix

  - dig
  - finger
  - getent
  - host
  - hostname
  - ifconfig
  - ip
  - netstat

    - `netstat -tunp`
    - `netstat -natup`

    - Socket table

      - Linux

        - `netstat -nat`

      - Solaris

        - `netstat -an -P tcp`

  - nslookup

  - ping/traceroute

  - route

  - ss
  - telnet

## Network configuration files

![image](Resources/Networking and Name Resolution/Network Config File Locations.PNG)

## Networking Utilities

- ipconfig
- arp
- ping/traceroute
- netstat
- route

## Name resolution

- nsswitch.conf
- resolv.conf
- hosts

## Network File Systems

- NFS original
- mountd
- nlockmgr -
